package majority;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.eclipseoptions.javatest.majority.api.MajorityElementFinderImpl_UsingHashMap;

public class MajorityElementFinderImpl_UsingHashMapTest {

	@Test
	public void test() {
//		fail("Not yet implemented");
		
		int[] numbers = {2,8,5,12,12,12,12,12,12,12,13};
//		int[] numbers = {12,12};
//		int[] numbers = {12,12,13,13,12};
		
		MajorityElementFinderImpl_UsingHashMap obj = 
				new MajorityElementFinderImpl_UsingHashMap();
		
		int result = obj.majorityElement(numbers);
		
		assertTrue(result == 12);
	}
	
}
