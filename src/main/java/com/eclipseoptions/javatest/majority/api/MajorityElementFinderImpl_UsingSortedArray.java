package com.eclipseoptions.javatest.majority.api;

import java.util.Arrays;

/**
 * Problem Statement:
 * 
 * For this test, given an array of n integers, 
 * find the element that appears more than n/2 times.
 * 
 * You can assume that the array is non-empty and that an 
 * element will appear more than n/2 times.
 * 
 * You can offer multiple implementations with differing 
 * performance characteristics if you see fit.
 * 
 * 
 * @author ashwin srivastava
 *
 */

public class MajorityElementFinderImpl_UsingSortedArray implements MajorityElementFinder {

	
	/**
	 *  Assuming that only element exists that
	 *  appears more than n/2 times.
	 * 
	 *  
	 */
	
	public int majorityElement(int[] numbers) {
		
		int result = 0;
		int len = numbers.length; 
		int count = 1;
		boolean flag = true;
		  
		Arrays.sort(numbers);
		
		for(int i=0; i<len-1; i++) {
			
			int curr = numbers[i];
			int next = numbers[i+1];
			
			if(curr == next) {
				count++;
			}else {
				if(count > len/2) {
					result = curr;
					flag = false;
					break;
				}
				count = 1;
			}
		}
		
		if(flag && count> 1) {
			result = numbers[len-1];
		}
		return result;
		
	}

}
