package com.eclipseoptions.javatest.majority.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Problem Statement:
 * 
 * For this test, given an array of n integers, 
 * find the element that appears more than n/2 times.
 * 
 * You can assume that the array is non-empty and that an 
 * element will appear more than n/2 times.
 * 
 * You can offer multiple implementations with differing 
 * performance characteristics if you see fit.
 * 
 * 
 * @author ashwin srivastava
 *
 */

public class MajorityElementFinderImpl_UsingHashMap implements MajorityElementFinder {

	
	/**
	 *  Assuming that only element exists that
	 *  appears more than n/2 times.
	 * 
	 *  
	 */
	
	public int majorityElement(int[] numbers) {
		// TODO Auto-generated method stub
		Map<Integer, Integer> elementCount = new HashMap<Integer, Integer>();
		
		int len = numbers.length;
		
		int result = 0;
		// if more than one such element exists then storing them in
		// the resultList and return resultList.get(0) OR resultList.get(size-1)
		// where size = resultList.size()
		List<Integer> resultList = new ArrayList<Integer>(); 
	
		if(len == 0) {
			throw new IllegalArgumentException("Array should be NON-EMPTY");
		}
		
		for(int n: numbers) {
			if(elementCount.containsKey(n)) {
				int count = elementCount.get(n);
				count++;
				elementCount.put(n, count);
			}else {
				elementCount.put(n, 1);
			}
		}
		
		for(int elem: elementCount.keySet()) {
			
			int count = elementCount.get(elem);
			if(count > len/2) {
				result = elem;
				resultList.add(elem);
			}
		}
		
		return result;
//		 return resultList.get(0);
//		 return resultList.get(resultList.size()-1);
	}

}
